package rogoz.patrick.lab11.Ex1;

import java.awt.FlowLayout;
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.*;

public class TextView extends JPanel implements Observer{

    JTextField jtfTemp;
    JLabel jtlTemp;
    JButton action;

    TextView(){
        this.setLayout(new FlowLayout());
        jtfTemp = new JTextField(20);
        jtlTemp = new JLabel("Temperature");
        action = new JButton("Enable-Disable");
        add(action);add(jtlTemp);add(jtfTemp);
    }

    public void update(Observable o, Object arg) {
        String s = ""+((Thermometer)o).getTemperature();
        jtfTemp.setText(s);
    }

    public void addEnableDisableListener(TemperatureController.EnableDisableListener listener) {
        action.addActionListener(listener);
    }
}



class TemperatureCanvasView extends JPanel implements Observer{

    private static final int width = 20;
    private static final int top = 20;
    private static final int left = 100;
    private static final int right = 250;
    private static final int height = 200;

    private double crtTemp;

    public void paintComponent(Graphics g){
        super.paintComponent(g);
        g.setColor(Color.black);
        g.drawRect(left,top, width, height);
        g.setColor(Color.red);
        g.fillOval(left-width/2, top+height-width/3,width*2, width*2);
        g.setColor(Color.black);
        g.drawOval(left-width/2, top+height-width/3,width*2, width*2);
        g.setColor(Color.white);
        g.fillRect(left+1,top+1, width-1, height-1);
        g.setColor(Color.red);
        long redtop = (long)(height*(crtTemp-Thermometer.MAX_VALUE)/(Thermometer.MIN_VALUE-Thermometer.MAX_VALUE));
        g.fillRect(left+1, top + (int)redtop, width-1, height-(int)redtop);
        g.setColor(Color.BLUE);
    }

    public void update(Observable o, Object arg) {
        crtTemp = ((Thermometer)o).getTemperature();
        repaint();
    }
}


class TemperatureController  {
    Thermometer t;
    TextView tview;
    public TemperatureController(Thermometer t, TextView tview, TemperatureCanvasView tcanvasView){
        t.addObserver(tview);
        t.addObserver(tcanvasView);
        this.t = t;
        this.tview = tview;

        tview.addEnableDisableListener(new EnableDisableListener());
    }

    class EnableDisableListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            t.setPause(!t.isPaused());
        }

    }
}


class TemperatureApp extends JFrame{

    TemperatureApp(TextView tview, TemperatureCanvasView tcanvasView){
        setLayout(new BorderLayout());
        tcanvasView.setPreferredSize(new Dimension(300,300));
        add(tview,BorderLayout.NORTH);
        add(tcanvasView,BorderLayout.CENTER);
        pack();
        setVisible(true);
    }

    public static void main(String[] args) {
        Thermometer t = new Thermometer();
        t.start();

        TemperatureCanvasView tcanvasView = new TemperatureCanvasView();
        TextView tview = new TextView();
        TemperatureController tcontroler = new TemperatureController(t,tview,tcanvasView);

        new TemperatureApp(tview,tcanvasView);
    }
}
